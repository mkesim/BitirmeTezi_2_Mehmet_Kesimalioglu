const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const bcrypt = require('bcryptjs');
const app = express();
const PORT = 3000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

let db = new sqlite3.Database('./database.db');

app.get('/', (req, res) => {
  // Dosya yolu düzeltildi
  res.sendFile(__dirname + '/index.html');
});

app.post('/login', (req, res) => {
  const { email, password } = req.body;
  // SQL sorgusu string olarak işaretlendi
  db.get("SELECT * FROM users WHERE email = ?", [email], (err, user) => {
    if (err) {
      res.send('An error occurred');
      return console.error(err.message);
    }
    if (user) {
      bcrypt.compare(password, user.password, (err, result) => {
        if (result) {
          // Dosya yolu düzeltildi
          res.sendFile(__dirname + '/anasayfa.html');
        } else {
          res.send('Şifre hatalı');
        }
      });
    } else {
      res.send('Kullanıcı bulunamadı');
    }
  });
});

app.listen(PORT, () => {
  // console.log içindeki metin tırnak işaretleri içine alındı ve template literal kullanıldı
  console.log(`Server is running on http://localhost:${PORT}`);
});