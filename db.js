const sqlite3 = require('sqlite3').verbose();
const bcrypt = require('bcryptjs');

// Veritabanı bağlantısını aç
let db = new sqlite3.Database('./database.db', (err) => {
  if (err) {
    console.error(err.message);
    return;
  }
  console.log('Veritabanına bağlanıldı.');
});

// Veritabanında users tablosunu oluştur
db.run(`CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  email TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL
)`, [], function(err) {
  if (err) {
    console.error(err.message);
    return;
  }
  console.log('Users tablosu oluşturuldu veya zaten mevcut.');
  
  // Kullanıcı bilgileri
  const email = 'yakupkutlu@gmail.com';
  const password = '123456';

  // Şifreyi hash'le
  const salt = bcrypt.genSaltSync(10);
  const hashedPassword = bcrypt.hashSync(password, salt);

  // Kullanıcıyı veritabanına ekle
  db.run(`INSERT INTO users (email, password) VALUES (?, ?)`, [email, hashedPassword], function(err) {
    if (err) {
      console.error(err.message);
      return;
    }
    // Ekleme işlemi başarılı
    console.log(`Bir kullanıcı eklendi: ${this.lastID}`);
  });
});

// Veritabanı bağlantısını kapat
// Not: Bu işlemi genellikle uygulamanın sonunda yapmak istersiniz.
// db.close((err) => {
//   if (err) {
//     console.error(err.message);
//   }
//   console.log('Veritabanı bağlantısı kapatıldı.');
// });